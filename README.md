DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    cli/                 contains angular 2 cli for client
        e2e/             end-to-end test
        node_modules/    angular library
        src/             angular source code          
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```
GLOBAL INSTALLATION
-------------------

```
curl -L https://www.npmjs.com/install.sh | sh 
sudo apt-get install npm
npm -v //require >=3.
sudo npm cache clean -f
sudo npm install -g n
sudo n stable
nodejs -v // >=4.x
sudo npm install --save-dev webpack
sudo npm install rxjs -g
sudo npm install -g @angular/cli@latest

```

GLOBAL INSTALLATION
-------------------

```
ng build                build to web/dist
ng build --watch        build to web/dist, watch and update source code after modify    
ng build --prod         build to web/dist with product environment & minified js file

```