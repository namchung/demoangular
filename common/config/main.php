<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
        //            'baseUrl' => '/foresta/frontend/web',
                    'class' => 'yii\web\UrlManager',
                    // Disable index.php
                    'showScriptName' => false,
                    // Disable r= routes
                    'enablePrettyUrl' => true,
                    'rules' => [
//                        'OPTIONS api/user/login' => 'api/user/options',
                        '*' => '/',
                        '<controller:[\w\-]+>/<id:\d+>' => '<controller>/view',
                        '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>',
                        '<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',
                        '<module:[\w\-]+>/<controller:user>/<id:\d+>/<action:[\w\-]+>' => '<module>/<controller>/<action>',
                        '<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                        '<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>/<offer_id:\d+>' => '<module>/<controller>/<action>',
                        '<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>'=>'<module>/<controller>/<action>',

                    ]
                ],
    ],
];
