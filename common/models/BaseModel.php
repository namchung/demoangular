<?php
/**
 * Created by PhpStorm.
 * User: chungtn
 * Date: 08/02/2017
 * Time: 14:45
 */

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{
    const ACTIVE = 0;
    const DELETED = 1;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('U'),
            ],
        ];
    }

    /**
     * @param $id
     * @param int $deleteValue
     * @return bool
     */
    public static function safeDelete($id, $deleteValue = self::DELETED)
    {
        /** @var self $model */
        $model = self::find()->where(['id' => $id])->one();
        if (!$model) {
            return false;
        }
        try {
            $transaction = Yii::$app->db->beginTransaction();
            if (!$model->afterSafeDelete()) {
                $transaction->rollBack();
            }
            $model->updateAttributes(['deleted' => $deleteValue]);
            $transaction->commit();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function afterSafeDelete()
    {
        return true;
    }

    /**
     * @param null $condition
     * @return ActiveQuery
     */
    public static function findActive($condition = null)
    {
        /** @var $query ActiveQuery   */
        $query = self::find()->where(['deleted' => self::ACTIVE]);
        if ($condition) {
            $query->andWhere($condition);
        }

        return $query;
    }

    public function generateUniqueRandomString($attribute, $length = 32)
    {

        $randomString = Yii::$app->getSecurity()->generateRandomString($length);

        if (!$this->findOne([$attribute => $randomString]))
            return $randomString;
        else
            return $this->generateUniqueRandomString($attribute, $length);

    }
}