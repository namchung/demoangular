<?php

namespace common\models;

use common\models\DtbFollow;
use common\models\DtbTeacher;
use Yii;
use yii\authclient\ClientInterface;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\base\Model;


/**
 * User model
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $access_token
 * @property integer $created_at
 * @property integer $updated_at
 */
class UserProvider extends BaseModel
{

    const FACEBOOK = 1;
    const TWITTER = 2;
    const GOOGLE = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_provider';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type','user_id','provider_id'],'integer'],
        ];
    }


    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->onCondition(['user.deleted'=>0]);
    }

    public function login(){
        return User::login($this->user_id);
    }


    public static function clientAuth($client)
    {
        /** @var ClientInterface $client */
        $attributes = $client->getUserAttributes();
        $provider_id=$attributes['id'];
        $clientName=$client->getName();
        $attributes['clientName']=$clientName;
        switch ($clientName){
            case 'facebook':
                $attributes['firstName']=$attributes['first_name'];
                $attributes['lastName']=$attributes['last_name'];
                $attributes['username']=$attributes['name'];
                unset($attributes['first_name'],$attributes['last_name'],$attributes['name']);
                $type= self::FACEBOOK;break;
            case 'twitter':
                $attributes['username']=$attributes['name'];
                unset($attributes['name']);

                $type= self::TWITTER;break;
            case 'google':
                $attributes['firstName']=$attributes['name']['givenName'];
                $attributes['lastName']=$attributes['name']['familyName'];
                $attributes['username']=$attributes['emails'][0]['value'];
                unset($attributes['name'],$attributes['emails']);
                $type= self::GOOGLE;break;
            default:
                return false;
        }
        $provider = self::findActive(['type' => $type, 'provider_id' => $provider_id])->one();
        /** @var self $provider */

        $user_id=Yii::$app->user->id;
        if ($user_id) {
            if (!$provider) {
                if (self::newModel($user_id,$type)) {
                    return ['action' => 'bind'];
                }
            }
        }
        else {
            if ($provider) {
                if ($userInfo=$provider->login()) {
                    return ['action' => 'login', 'user' => $userInfo];
                }
            } else {
                return ['action' => 'register','user'=>$attributes];
            }
        }
        return false;

    }
    public static function newModel($user_id,$type,$provider_id=null){
        if ($model=self::findActive(['user_id'=>$user_id,'type'=>$type])->one()){
            return true;
        }
        $model=new self();
        $model->user_id=$user_id;
        $model->type=$type;
        $model->provider_id=$provider_id;
        return $model->save();
    }

}
