<?php

namespace common\models;

use common\models\DtbFollow;
use common\models\DtbTeacher;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\base\Model;


/**
 * User model
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $access_token
 * @property integer $created_at
 * @property integer $updated_at
 */
class UserToken extends BaseModel
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_token';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->onCondition(['user.deleted'=>0]);
    }

    public static function newToken($user_id){
        $user_token = new UserToken;
        $user_token->user_id = $user_id;
        $user_token->access_token = $user_token->generateUniqueRandomString('access_token');
        if ($user_token->save()){
            return $user_token->access_token;
        }
        return false;
    }
}
