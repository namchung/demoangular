<?php

use yii\db\Migration;

class m170213_103820_create_user_provider extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('user_provider', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'type'=>$this->integer(),
            'provider_id'=>$this->bigInteger(),
            'deleted'=> $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('user_provider');
    }
}
