﻿import { Component, ChangeDetectorRef } from '@angular/core';
import {Injector} from "@angular/core";
import { User } from '../_models';
import { UserService } from '../_services/index';
import { Router, ActivatedRoute } from '@angular/router';
import {InjectorService} from '../base-services/injector-service';

export class BaseComponent {

    protected route: ActivatedRoute;
    protected router: Router;
    // protected chRef: ChangeDetectorRef;
    constructor(injector: Injector) {
        this.route = injector.get(ActivatedRoute);
        this.router = injector.get(Router);
        // this.chRef = injector.get(ChangeDetectorRef);
    }

}
