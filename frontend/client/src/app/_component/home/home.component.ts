﻿import { Component, OnInit,DoCheck } from '@angular/core';

import { User } from '../../_models';
import { UserService } from '../../_services/index';
import { BaseComponent } from '../base.component';
import {Injector} from "@angular/core";

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})

export class HomeComponent extends BaseComponent implements OnInit, DoCheck {
    currentUser: User;
    users: User[] = [];
    returnUrl: string;

    constructor(injector:Injector,private userService: UserService) {
        super(injector);
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';

    }

    ngOnInit() {
        this.loadAllUsers();
    }

    deleteUser(id: number) {
        this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
    }

    private loadAllUsers() {
        if (!this.currentUser){
            this.router.navigate([this.returnUrl]);
        } else
        this.userService.getAll().subscribe(users => { this.users = users;});
    }
    ngDoCheck() {
       // console.log(this.users);
    }
}
