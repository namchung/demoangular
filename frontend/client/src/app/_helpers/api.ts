﻿import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { Injectable } from '@angular/core';
import { RequestOptions, RequestOptionsArgs } from '@angular/http';
import { BaseHttp } from '../base-services/base-http';

@Injectable()
export class ApiRequestOptions extends BaseRequestOptions {
  // merge(options?:RequestOptionsArgs):RequestOptions {
  //   options.url = 'http://foresta.com' + options.url;
  //   return super.merge(options);
  // }
}
