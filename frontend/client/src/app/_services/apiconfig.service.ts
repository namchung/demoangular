import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class ApiConfigService {
  private apiUrl: string = "";

  constructor(private _http: Http) { }

  public load() {
    return new Promise((resolve, reject) => {
      console.debug('Retrieving configuration settings...');
      this._http.get('environments/environment.ts')
        .map(res => res.json())
        .subscribe(
          (data: any) => {
              console.log(data);
              console.debug('Configuration settings retrieved as: ', data);
            this.apiUrl = data.apiUrl;
            resolve(true);
          },
          err => console.error(err)
        );
    });
  }

  public getApiBaseUrl() {
    return this.apiUrl;
  }
}
