﻿import { Injectable } from '@angular/core';
import { Http, Headers, BaseRequestOptions,RequestOptions, Response, RequestOptionsArgs } from '@angular/http';
import { User } from '../_models';
import { ApiRequestOptions } from '../_helpers';
import {root} from "rxjs/util/root";
import {BaseHttp} from "../base-services/base-http";

@Injectable()
export class UserService extends BaseRequestOptions{
  constructor(private http: BaseHttp) {
    super();
  }

  getAll() {
    return this.http.get('/api/user/list').map((response: Response) => response.json());
  }

  getById(id: number) {
    return this.http.get('/api/user/profile/' + id).map((response: Response) => response.json());
  }

  create(user: User) {
    return this.http.post('/api/user/register', user).map((response: Response) => response.json());
  }

  update(user: User) {
    return this.http.put('/api/user/' + user.id, user).map((response: Response) => response.json());
  }

  delete(id: number) {
    return this.http.delete('/api/user/safe-delete/' + id).map((response: Response) => response.json());
  }

  // private helper methods

}
