import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './_guards/index';
import {TodoComponent} from './_component/todo/todo.component';
import {HomeComponent} from './_component/home';
import {LoginComponent} from './_component/login';
import {RegisterComponent} from './_component/register';
import {AuthComponent} from './_directives/authLogin';


const routes: Routes = [

  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  {path: ':status', component: TodoComponent},
  {path: '**', redirectTo: '/home'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routedComponents = [HomeComponent, LoginComponent, RegisterComponent,TodoComponent,AuthComponent                                        ];
