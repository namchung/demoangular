import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {BaseRequestOptions,HttpModule} from '@angular/http';
import {BaseHttp} from './base-services/base-http';
import { MaterialModule } from '@angular/material';
import {AlertService, AuthenticationService, UserService,WindowRef} from './_services/index';
import {AuthGuard} from './_guards/index';
import { AppRoutingModule, routedComponents } from './app-routing.module';
import { AlertModule } from 'ng2-bootstrap';

@NgModule({
    declarations: [
        AppComponent,
        routedComponents
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        MaterialModule,
        AppRoutingModule,
        AlertModule.forRoot()
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        BaseHttp,
        BaseRequestOptions,
        WindowRef
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
