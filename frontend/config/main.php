<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'application/x-www-form-urlencoded' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
               '*' => '/',
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    'attributeParams' => [
                        'include_email' => 'true'
                    ],
                    'consumerKey' => 'FiVfqCzBD3nxaFPEUPQFH3S1O',
                    'consumerSecret' => 'a3YYf5Kk9LCK1Qk02hcib4s9YGkag3J5nVchFt9OpgZ6VJBOCP',
                ],

                'facebook' => [
                    'authUrl'      => 'https://www.facebook.com/dialog/oauth',
                    'class'        => 'yii\authclient\clients\Facebook',
                    'clientId'     => '413777212300834',
                    'clientSecret' => 'eee5cd99cc883f3f0ddbd0698d69c3fa',
                    'scope'        => [
                        'email',
                        'public_profile',
                        'user_about_me',
                        'user_location',
                        'user_work_history',
                    ],
                    'attributeNames'=>[
                        'name',
                        'email',
                        'first_name',
                        'gender',
                        'last_name'
                    ]
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '201672825346-u4h39d8kiqedp5coeq7rhr40cjavolbp.apps.googleusercontent.com',
                    'clientSecret' => 'JT4PN76IIou9bWTOSDEHK8d8',
                ],
            ],
        ]
    ],
     'modules' => [
            'api' => [
                'class' => 'app\module\api\Module',
            ],
        ],
    'params' => $params,
];
