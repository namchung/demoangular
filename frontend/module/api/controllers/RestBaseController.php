<?php

namespace app\module\api\controllers;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

class RestBaseController extends ActiveController {

    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    public $modelClass = '';

    public function actions(){
        $actions = [
            'search' => [
                'class'       => 'app\module\rest\actions\SearchAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'params'      => \Yii::$app->request->get()
            ],
        ];
        $parentActions = parent::actions();
        unset($parentActions['index']);
        return array_merge($parentActions, $actions);
    }
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
    public function actionSearch()
    {
        if (!empty($_GET)) {
            $model = new $this->modelClass;
            foreach ($_GET as $key => $value) {
                if (!$model->hasAttribute($key)) {
                    throw new \yii\web\HttpException(404, 'Invalid attribute:' . $key);
                }
            }
            try {
                $provider = new ActiveDataProvider([
                    'query' => $model->find()->where($_GET),
//                    'pagination' => false
                ]);
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(500, 'Internal server error');
            }

            if ($provider->getCount() <= 0) {
                throw new \yii\web\HttpException(404, 'No entries found with this query string');
            } else {
                return $provider;
            }
        } else {
            throw new \yii\web\HttpException(400, 'There are no query string');
        }
    }

    public function responseWithCode($data, $code = 200)
    {
        \Yii::$app->response->setStatusCode($code);
        return $data;
    }
} 