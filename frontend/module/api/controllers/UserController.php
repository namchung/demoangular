<?php
namespace app\module\api\controllers;

use common\models\User;
use common\models\UserToken;
use yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\SignupForm;
use yii\authclient\OAuth2;

class UserController extends RestBaseController
{


    public function actionLogin()
    {

        $username = Yii::$app->request->post('username');
        $password = Yii::$app->request->post('password');
        $model = new LoginForm();
        $model->load(Yii::$app->request->post(), '');
        if (!$model->validate()) {
            // validation fails
            return $this->responseWithCode($model->firstErrors, 400);
        } else {
            // validation succeeds
            /** @var User $user */
            $user = User::findByUsername($username);
            if ($user) {
                if (!$user->validatePassword($password)) {
                    return $this->responseWithCode(["password" => "password is not match"], 400);
                } else {
                   if ($login=User::login($user->id)){
                       return $login;
                   }
                    return $this->responseWithCode(["error" => "Failed to login"], 500);
                }
            } else {
                return $this->responseWithCode(["username" => "username has not found"], 404);
            }

        }

    }

    public function actionLogout()
    {
        User::logout();
        return ["message" => "Logout successfully"];
    }

    public function actionForgotPassword()
    {
        $model = new PasswordResetRequestForm();
        $a = $model->email = Yii::$app->request->post('email');
        if ($model->validate()) {
            if ($model->sendEmail()) {
                return $this->responseWithCode(["message" => "We've sent you an email with instructions on how to reset your password."]);
            } else {
                return $this->responseWithCode(["message" => "We can't send you an email now, please try again later"], 404);
            }
        } else {
            return $this->responseWithCode($model->firstErrors, 400);
        }
    }

    public function actionRegister()
    {
        $model = new SignupForm();
        $model->load(Yii::$app->request->post(), '');
        if ($model->validate()) {
            if ($user = $model->signup()) {
                return ["message" => "Register user successfully"];
            } else {
                return $this->responseWithCode(["message" => "system error"], 404);
            }
        } else {
            return $this->responseWithCode($model->firstErrors, 400);
        }

    }

    public function actionProfile($id)
    {
        return User::findOne($id);
    }

    public function actionList()
    {
        if (Yii::$app->user->isGuest){
            return $this->responseWithCode(400,'login required');
        }
        return User::find()->where(['deleted' => User::ACTIVE])->limit(5)->all();
    }

    public function actionSafeDelete($id)
    {
        return User::safeDelete($id);
    }

}



